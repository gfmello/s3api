package uk.ac.ebi.sdo.s3.fire.service;

import org.springframework.core.io.Resource;
import uk.ac.ebi.sdo.s3.fire.model.ListAllMyBucketsResult;
import uk.ac.ebi.sdo.s3.fire.model.ListBucketResult;
import uk.ac.ebi.sdo.s3.fire.model.S3Object;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is the abstraction interface to implement your own repository
 *
 * @author Ralf Ulrich on 17.02.16.
 */
public interface S3Service {


    ListAllMyBucketsResult listAllBuckets(HttpServletRequest req, HttpServletResponse res);

    ListBucketResult listBucket(HttpServletRequest req, HttpServletResponse res, String bucket, String prefix);

    Object getObject(HttpServletRequest req, HttpServletResponse res, String bucket, String objectKey);

    void headBucket(HttpServletRequest req, HttpServletResponse res, String bucket);

    void headObject(HttpServletRequest req, HttpServletResponse res, String bucket, String objectKey);

    S3Object putObject(String bucket, String objectKey, String contentLength, String md5, Resource in);
}
