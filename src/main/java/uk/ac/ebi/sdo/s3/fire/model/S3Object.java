package uk.ac.ebi.sdo.s3.fire.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.InputStream;
import java.util.Date;

@Data
@Accessors(chain = true)
public class S3Object {

    private String key;
    private Date lastModified;

    // S3User Owner;

    private String eTag;

    private String versionId;

    private Long size;

    private String mimeType;

    private String storageClass;

    private String bucket;

    private boolean deleted;

    // S3Metadata Metadata;

    private InputStream content;

    private boolean latest;
}
