package uk.ac.ebi.sdo.s3.fire.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import uk.ac.ebi.sdo.s3.fire.communication.model.Archive;
import uk.ac.ebi.sdo.s3.fire.communication.model.ArchiveObject;
import uk.ac.ebi.sdo.s3.fire.communication.model.Dentry;
import uk.ac.ebi.sdo.s3.fire.communication.service.SupermigCallerService;
import uk.ac.ebi.sdo.s3.fire.model.Bucket;
import uk.ac.ebi.sdo.s3.fire.model.Content;
import uk.ac.ebi.sdo.s3.fire.model.ListAllMyBucketsResult;
import uk.ac.ebi.sdo.s3.fire.model.ListBucketResult;
import uk.ac.ebi.sdo.s3.fire.model.Owner;
import uk.ac.ebi.sdo.s3.fire.model.Prefix;
import uk.ac.ebi.sdo.s3.fire.model.S3Object;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.ZoneOffset.UTC;
import static uk.ac.ebi.sdo.s3.fire.service.Constants.PRIVATE_BUCKET_SUFFIX;
import static uk.ac.ebi.sdo.s3.fire.service.Constants.PUBLIC_BUCKET_SUFFIX;

@Slf4j
@Service
public class S3ServiceImpl implements S3Service {

    @Autowired
    private SupermigCallerService supermigCallerService;

    @Override
    public ListAllMyBucketsResult listAllBuckets(HttpServletRequest req, HttpServletResponse res) {
        List<Archive> archiveList = supermigCallerService.getAllArchives();
        List<Bucket> bucketList = archiveList.stream()
                .map(a -> new Bucket().setCreationDate(a.getCreateTime()).setName(a.getArchiveName() + PUBLIC_BUCKET_SUFFIX))
                .collect(Collectors.toList());

        String withPrivateBucketArchive = SecurityHelper.getArchiveFromCurrentUser();
        if (withPrivateBucketArchive != null) {
            Archive userArchive = archiveList.stream().filter(a -> a.getArchiveName()
                    .equals(withPrivateBucketArchive)).findFirst().orElseThrow();
            Bucket privateBucket = new Bucket()
                    .setCreationDate(userArchive.getCreateTime())
                    .setName(userArchive.getArchiveName() + PRIVATE_BUCKET_SUFFIX);
            bucketList.add(privateBucket);
        }

        Owner owner = new Owner().setDisplayName("fire").setId("fire");

        return new ListAllMyBucketsResult()
                .setBuckets(bucketList)
                .setOwner(owner);
    }

    @Override
    public ListBucketResult listBucket(HttpServletRequest req, HttpServletResponse res, String bucket, String prefix) {
        List<Dentry> dentryList = supermigCallerService.listBucket(bucket, prefix);
        // if (StringUtils.hasLength(prefix)) {
        // archiveObjectList = archiveObjectList.stream().filter(ao -> ao.getFilesystemEntry().getPath().startsWith("/" + prefix)).collect(Collectors.toList());
        // }
        return dentryListToListBucketResult(dentryList, "/", bucket);
    }

    @Override
    public Object getObject(HttpServletRequest req, HttpServletResponse res, String bucket, String objectKey) {
        res.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        return new InputStreamResource(supermigCallerService.getBlob(bucket, objectKey));
    }

    @Override
    public void headBucket(HttpServletRequest req, HttpServletResponse res, String bucket) {
    }

    @Override
    public void headObject(HttpServletRequest req, HttpServletResponse res, String bucket, String objectKey) {
        res.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        res.setContentLength(12);
        res.addHeader(HttpHeaders.EXPIRES, DateTimeFormatter.RFC_1123_DATE_TIME
                .format(Instant.now().atZone(UTC).plusMinutes(60)));
        res.addHeader(HttpHeaders.LAST_MODIFIED, DateTimeFormatter.RFC_1123_DATE_TIME
                .format(LocalDateTime.now().atZone(UTC)));
        res.addHeader(HttpHeaders.ACCEPT_RANGES, "bytes");
    }

    @Override
    public S3Object putObject(String bucket, String objectKey, String contentLength, String md5, Resource in) {
        ArchiveObject archiveObject = supermigCallerService.submitObject(bucket, objectKey, contentLength, md5, in);
        archiveObject = supermigCallerService.setObjectPath(archiveObject.getFireOid(), objectKey);
        return new S3Object()
                .setKey(objectKey)
                .setSize(archiveObject.getObjectSize())
                .setLastModified(Date.from(archiveObject.getCreateTime().atZone(ZoneId.systemDefault()).toInstant()))
                .setBucket(bucket)
                .setDeleted(false)
                .setLatest(true)
                .setETag(archiveObject.getObjectMd5())
                .setMimeType(MediaType.APPLICATION_OCTET_STREAM_VALUE)
                ;
    }

    public static ListBucketResult dentryListToListBucketResult(List<Dentry> dentryList, String path,
                                                                String bucketName) {
        List<Prefix> prefixes = new ArrayList<>();
        List<Content> contents = new ArrayList<>();
        path = path.equals("/") ? "" : path;
        for (Dentry dentry : dentryList) {
            if (dentry.getFireOid() == null) {
                maybeAddPrefix(prefixes, dentry, path);
            } else {
                contents.add(getContent(dentry, path));
            }
        }

        return new ListBucketResult()
                .setPrefixes(prefixes)
                .setContents(contents)
                .setDelimiter("/")
                .setKeyCount(contents.size() + prefixes.size())
                .setName(bucketName)
                .setPrefix(path)
                // .setTruncated(continuationToken.isTruncated());
                ;
    }

    private static Content getContent(Dentry dentry, String path) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String date = dentry.getModifyTime().format(formatter);
        return new Content()
                .setEtag(dentry.getObjectMd5())
                .setKey(path.endsWith(dentry.getDentryName()) ? dentry.getDentryName() : path + dentry.getDentryName())
                .setLastModified(date)
                .setSize(dentry.getObjectSize())
                .setStorageClass("STANDARD");
    }

    private static void maybeAddPrefix(List<Prefix> prefixList, Dentry dentry, String path) {
        //aws expects directories to have trailing slash
        //if this dentry is at the same path as the request we don't need to add the dentry
        //if path request doesn't have a trailing slash then we need to add this dentry as a commonPrefix
        //even if it's at the same location
        String dentryPath = dentry.getPath() == null ? "" : dentry.getPath();
        path = path.endsWith("/") ? path : path + "/";
        //  if (!path.endsWith("/") && path.equals(dentryPath)) {
        //       prefixList.add(new Prefix().setPrefix(path + "/"));
        //   }
        //  if (!dentryPath.equals(path)) {
        prefixList.add(new Prefix().setPrefix(path + dentry.getDentryName() + "/"));
        // }
    }

}
