package uk.ac.ebi.sdo.s3.fire.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Prefix {

    @JacksonXmlProperty(localName = "Prefix")
    private String prefix;
}
