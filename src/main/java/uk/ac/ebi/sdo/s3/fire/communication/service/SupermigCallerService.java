package uk.ac.ebi.sdo.s3.fire.communication.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import uk.ac.ebi.sdo.s3.fire.communication.model.Archive;
import uk.ac.ebi.sdo.s3.fire.communication.model.ArchiveObject;
import uk.ac.ebi.sdo.s3.fire.communication.model.Dentry;

import java.io.InputStream;
import java.util.List;

public interface SupermigCallerService {
    List<Archive> getAllArchives();

    List<Dentry> listBucket(String bucket, String prefix);

    InputStream getBlob(String bucket, String objectKey);

    ArchiveObject submitObject(String bucket, String objectKey, String contentLength, String md5, Resource in);

    ArchiveObject setObjectPath(String fireOid, String objectKey);
}
