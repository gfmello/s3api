package uk.ac.ebi.sdo.s3.fire.model;

import lombok.Data;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Component;

@Data
@Component
public class ContinuationToken {

    private static final String PAGE_DELIMITER = "#";
    private String randomRoot = RandomString.make(10);
    private boolean truncated = false;
    private int pageNo = 0;

    public ContinuationToken() {
    }

    public ContinuationToken(String continuationToken) {

        if (continuationToken == null || continuationToken.isEmpty()) {
            return;
        }
        String[] enc = continuationToken.split(PAGE_DELIMITER);
        randomRoot = enc[0];
        pageNo = Integer.parseInt(enc[1]);
    }

    public static void incrementContinuationTokenPage(ContinuationToken continuationToken) {
        continuationToken.setTruncated(true);
        continuationToken.setPageNo(continuationToken.getPageNo() + 1);
    }

    public String toString() {
        return this.randomRoot + PAGE_DELIMITER +
                this.pageNo;
    }

}
