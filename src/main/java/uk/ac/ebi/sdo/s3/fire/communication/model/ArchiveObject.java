package uk.ac.ebi.sdo.s3.fire.communication.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class ArchiveObject {
    private Long objectId;
    private String fireOid;
    private String objectMd5;
    private Long objectSize;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    private FilesystemEntry filesystemEntry;

    @Data
    public static class FilesystemEntry {
        private String path;
        private boolean published;
    }
}
