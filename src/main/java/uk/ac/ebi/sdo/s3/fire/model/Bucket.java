package uk.ac.ebi.sdo.s3.fire.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class Bucket {

    @JacksonXmlProperty(localName = "Name")
    String name;
    @JacksonXmlProperty(localName = "CreationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    Date creationDate;
}
