package uk.ac.ebi.sdo.s3.fire.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Content {

    @JacksonXmlProperty(localName = "Key")
    String key;
    @JacksonXmlProperty(localName = "LastModified")
    String lastModified;
    @JacksonXmlProperty(localName = "ETag")
    String etag;
    @JacksonXmlProperty(localName = "Size")
    Long size;
    @JacksonXmlProperty(localName = "StorageClass")
    String storageClass;
}
