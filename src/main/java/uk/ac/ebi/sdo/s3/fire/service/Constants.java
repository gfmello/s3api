package uk.ac.ebi.sdo.s3.fire.service;

public class Constants {

    public static final String PUBLIC_BUCKET_SUFFIX = "-public";
    public static final String PRIVATE_BUCKET_SUFFIX = "-private";

}
