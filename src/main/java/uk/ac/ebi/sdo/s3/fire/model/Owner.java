package uk.ac.ebi.sdo.s3.fire.model;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Owner {

    @JacksonXmlProperty(localName = "ID")
    private String id;
    @JacksonXmlProperty(localName = "DisplayName")
    private String displayName;
}
