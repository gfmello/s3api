package uk.ac.ebi.sdo.s3.fire.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@JacksonXmlRootElement(localName = "ListBucketResult")
public class ListBucketResult {

    @JacksonXmlProperty(localName = "Name")
    String name;
    @JacksonXmlProperty(localName = "Prefix")
    String prefix;
    @JacksonXmlProperty(localName = "KeyCount")
    int keyCount;
    @JacksonXmlProperty(localName = "MaxKeys")
    int maxKeys;
    @JacksonXmlProperty(localName = "Delimiter")
    String delimiter;
    @JacksonXmlProperty(localName = "IsTruncated")
    boolean truncated;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JacksonXmlProperty(localName = "NextContinuationToken")
    String nextContinuationToken;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Contents")
    private List<Content> contents;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CommonPrefixes")
    private List<Prefix> prefixes;

}
