package uk.ac.ebi.sdo.s3.fire.controller;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uk.ac.ebi.sdo.s3.fire.model.S3Object;
import uk.ac.ebi.sdo.s3.fire.service.S3Service;
import uk.ac.ebi.sdo.s3.fire.service.SecurityHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.Base64;
import java.util.NoSuchElementException;

@Slf4j
@RestController
public class S3Controller {

    @Autowired
    private S3Service s3Service;

    @GetMapping(value = "/**", produces = {MediaType.APPLICATION_XML_VALUE})
    public Object doGet(HttpServletRequest req, HttpServletResponse res) {
        String path = req.getServletPath();
        String bucket = getBucketName(path);
        String objectKey = getObjectKey(path, bucket);
        String prefix = req.getParameter("prefix");

        if (bucket == null) {
            return s3Service.listAllBuckets(req, res);
        } else if (objectKey == null) {
            return s3Service.listBucket(req, res, bucket, prefix);
        } else {
            return s3Service.getObject(req, res, bucket, objectKey);
        }
    }


    @RequestMapping(value = "/**", produces = {MediaType.APPLICATION_XML_VALUE}, method = RequestMethod.HEAD)
    public void doHead(HttpServletRequest req, HttpServletResponse res) {
        log.info("******* HEAD");
        String path = req.getServletPath();
        String bucket = getBucketName(path);
        String objectKey = getObjectKey(path, bucket);

        if (bucket != null) {
            if (objectKey == null) {
                s3Service.headBucket(req, res, bucket);
            } else {
                s3Service.headObject(req, res, bucket, objectKey);
            }
        }
    }

    @SneakyThrows
    @PutMapping(value = "/**", consumes = MediaType.ALL_VALUE, produces = {MediaType.APPLICATION_XML_VALUE})
    public S3Object doPut(HttpServletRequest req) {
        log.info("******* PUT");
        InputStreamResource in = new InputStreamResource(req.getInputStream());
        String path = req.getServletPath();
        String bucket = getBucketName(path);
        String objectKey = getObjectKey(path, bucket);
        if (bucket != null) {
            String contentLength = req.getHeader("Content-Length");
            String md5 = getMd5(req);
            return s3Service.putObject(bucket, objectKey, contentLength, md5, in);
        }
        throw new IllegalArgumentException("missing object key");
    }

    private String getMd5(HttpServletRequest req) {
        byte[] digest = Base64.getDecoder().decode(req.getHeader("Content-MD5"));
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }

    private String getBucketName(String servletPath) {
        if (servletPath != null) {
            String[] pathParts = servletPath.split("/");
            if (pathParts.length > 0) {
                String bucket = pathParts[1];
                if (!bucket.startsWith(SecurityHelper.getArchiveFromCurrentUser())) {
                    throw new NoSuchElementException("Bucket not found:" + bucket);
                }
                return bucket;
            }
        }
        return null;
    }

    private String getObjectKey(String servletPath, String bucket) {
        if (bucket != null && servletPath.length() > bucket.length() + 2) {
            return servletPath.substring(bucket.length() + 2);
        }
        return null;
    }

}
