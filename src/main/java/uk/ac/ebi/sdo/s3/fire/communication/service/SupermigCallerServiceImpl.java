package uk.ac.ebi.sdo.s3.fire.communication.service;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import uk.ac.ebi.sdo.s3.fire.communication.model.Archive;
import uk.ac.ebi.sdo.s3.fire.communication.model.ArchiveObject;
import uk.ac.ebi.sdo.s3.fire.communication.model.Dentry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;

@Service
public class SupermigCallerServiceImpl implements SupermigCallerService {

    private static final String SUPERMIG_HOST = "http://localhost:8080";
    private static final String SUPERMIG_ADMIN_URL = SUPERMIG_HOST + "/fire/admin/";

    private final RestTemplate restTemplate = new RestTemplate();


    @Override
    public List<Archive> getAllArchives() {
        ResponseEntity<List<Archive>> response = restTemplate.exchange(SUPERMIG_ADMIN_URL + "/archive",
                HttpMethod.GET,
                new HttpEntity<>(createAdminHeader()),
                new ParameterizedTypeReference<>() {
                });

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Error listing archives: " + response);
        }
        return response.getBody();
    }

    @Override
    public List<Dentry> listBucket(String bucket, String prefix) {
        String path = bucket + "/" + prefix == null ? "" : prefix;
        ResponseEntity<List<Dentry>> response = restTemplate.exchange(SUPERMIG_HOST + "/fire/admin/dentry/entries/path/" + path,
                HttpMethod.GET,
                new HttpEntity<>(createUserHeader()),
                new ParameterizedTypeReference<>() {
                });


        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Error listing archives: " + response);
        }

        return response.getBody();
    }

    @Override
    @SneakyThrows
    public InputStream getBlob(String bucket, String objectKey) {
        ResponseEntity<Resource> response = restTemplate.exchange(SUPERMIG_HOST + "/fire/objects/blob/path/" + objectKey,
                HttpMethod.GET,
                new HttpEntity<>(createUserHeader()), Resource.class);


        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Error listing archives: " + response);
        }

        return response.getBody().getInputStream();
    }

    @Override
    @SneakyThrows
    public ArchiveObject submitObject(String bucket, String objectKey, String contentLength, String md5, Resource in) {
        File tempFile = File.createTempFile(objectKey, "");
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in.getInputStream(), out);
        }
        HttpHeaders headers = createUserHeader();
        headers.set("X-FIRE-MD5", md5);
        headers.set("X-FIRE-Size", contentLength);

        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(tempFile));

        RequestEntity<MultiValueMap<String, Object>> request =
                RequestEntity.post(URI.create(SUPERMIG_HOST + "/fire/objects"))
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .headers(headers)
                        .body(body);

        ResponseEntity<ArchiveObject> response =
                restTemplate.exchange(request, ArchiveObject.class);

        Files.deleteIfExists(tempFile.toPath());

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Error listing archives: " + response);
        }

        return response.getBody();
    }

    @Override
    public ArchiveObject setObjectPath(String fireOid, String path) {
        HttpHeaders headers = createUserHeader();
        headers.set("X-FIRE-Path", path);

        ResponseEntity<ArchiveObject> response = restTemplate.exchange(SUPERMIG_HOST + "/fire/objects/" + fireOid + "/firePath",
                HttpMethod.PUT,
                new HttpEntity<>(headers), ArchiveObject.class);


        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Error setting firepath: " + response);
        }

        return response.getBody();
    }


    private HttpHeaders createAdminHeader() {
        return createHeaders("", "");
    }

    private HttpHeaders createUserHeader() {
        return createHeaders("", "");
    }

    private HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(
                    auth.getBytes(StandardCharsets.US_ASCII));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }


}
