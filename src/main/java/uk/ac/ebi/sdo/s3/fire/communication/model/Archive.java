package uk.ac.ebi.sdo.s3.fire.communication.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class Archive {
    private Long archiveId;
    private String archiveName;
    private Date createTime;
}