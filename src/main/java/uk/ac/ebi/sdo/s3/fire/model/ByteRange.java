package uk.ac.ebi.sdo.s3.fire.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ByteRange {
    private Long start;
    private Long end;
}
