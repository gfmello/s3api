package uk.ac.ebi.sdo.s3.fire.communication.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@Accessors(chain = true)
public class Dentry {
    private String fireOid;
    private String dentryName;
    private String objectMd5;
    private Long objectSize;
    private String path;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime modifyTime;

}
